package br.edu.up;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;

public class GeradorDeTopicos {
  
  public static void main(String[] args) throws Exception {
    
    //1. Cria��o do contexto, t�pico e factory;
    Context ctx = new InitialContext();
    Topic topico = (Topic) ctx.lookup("jms/TOPICO_DE_TESTE");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");
    
    
    //2. Cria��o da conex�o, sess�o e gerador;
    Connection con = cf.createConnection("up", "positivo");
    Session session  = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageProducer gerador = session.createProducer(topico);
    
    //3. Cria��o e envio de TextMessages;
    TextMessage msg = session.createTextMessage("Novo t�pico");
    gerador.send(msg);
    
    //4. Encerramento da conex�o e do contexto;
    con.close();
    ctx.close();
    
  }

}
